/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package ru.hiik.student;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.websocket.server.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


/**
 *
 * @author student
 */
@Path("/students")
public class StudentController {

    private static final Logger log = Logger.getLogger(StudentController.class.getName());
    
    
    
    @Inject
    StudentService service;
    
     
    /**
     * Получение полного списка студентов
     *
     * @return
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)   // Преобразует список в JSON массив
    public List<Student> getAllStudent() {
        log.log(Level.INFO, "запрос на получение полного списка студентов");
        List<Student> students = service.getAll();
        if (students != null) {
            log.log(Level.INFO, "Получен список студентов [" + students.size() + "] записей");
        }

        return students;
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Student create(Student student) {
        log.log(Level.INFO, "Поступил запрос на сохранение студента в базе данных");
        Student savedStudent = service.save(student);
        if (savedStudent != null)
        {
            log.log(Level.INFO, "Студент: [" + savedStudent + "] сохранен в базе данных");
        }
        return savedStudent;
    }
   
    /**
     * Метод уадаления студента из базы данных 
     * 
     * @param id
     * @param student
     * @return 
     */
    @DELETE
  
    @Path("{id}")
    @Transactional
    public void delete(@PathParam("id") Long id) {
        System.out.println("поступил запрос на удаление id= "+ id);
        Student entity = Student.findById(id);
        if(entity == null) {
            throw new NotFoundException();
        }
        entity.delete();
    }
     
}

